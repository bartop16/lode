/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lode.semestralka;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.util.logging.Level;
import java.util.logging.Logger;
import posluchace.*;
import network.*;
/**
 *
 * @author Petr Bartoš
 */
public class Plocha extends JPanel {
    
    public final int odr1 = 30; //odražení od kraje okna
    public final int odr2 = 310;
    public final int delka = 250;
    
    public int[][] radar = new int[10][10];  //mapa střel
    public int[][] flotila = new int[10][10];    //mapa lodí
    private Rozmistit rozmisteni;
    private Vystrel strelba;
    private boolean listeneryVymeneny = false;
    public boolean rozmisteno = false;
    public boolean hraciReady = false;
    public Lode hra;
    public Server soketS;
    public Klient soketK;
    public int pocetZasahu = 0;
    public Component zdrojovaKomponenta;
    
    public Plocha(Lode lod) {
        this.hra = lod;
        this.rozmisteni = new Rozmistit(this);
        this.strelba = new Vystrel(this);
        this.setPreferredSize(new Dimension(590, 300));
        this.setFont(new Font(Font.MONOSPACED, Font.BOLD, 15));
        this.addMouseMotionListener(rozmisteni);
        this.addMouseWheelListener(rozmisteni);
        this.addMouseListener(rozmisteni);
            /*  volba typu připojení    */
        String[] tlacitka = {"Server", "Klient"};
        int typ = JOptionPane.showOptionDialog(null, "Chcete vytvořit server, nebo se připojit jako klient?", "Síť",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, tlacitka, "");
        switch(typ){
            case 0:
                this.soketS = new Server(this, strelba);
                break;
            case 1:
                this.soketK = new Klient(this, strelba);
                break;
        }
    }
    
    public void strelaRad(int x, int y, int stav){
        if(stav == 2)
            radar[x][y] = stav;
        else
            radar[x][y] = stav + 1;
    }
    public void strelaFlot(int x, int y, int stav){
        flotila[x][y] = stav + 1;
        if(stav == 2)
            pocetZasahu++;
    }
    public void umistitLod(int x, int y){
        flotila[x][y] = 2;
    }
    public boolean konecHry(){
        return (pocetZasahu == 25);
    }
    public int stav(int x, int y){
        return flotila[x][y];
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
            /*  vykreslení hracích polí   */
        g.drawString("RADAR", odr1 + 4*delka/10, odr1 - 5);
        g.drawString("VAŠE FLOTILA", odr2 + 3*delka/10, odr1 - 5);
        g.setColor(Color.BLUE);
        g.fillRect(odr1, odr1, delka, delka); //vykreslení pozadí pole
        g.fillRect(odr2, odr1, delka, delka);
        g.setColor(Color.BLACK);
        g.drawRect(odr1, odr1, delka, delka); //vykreslení obvodu
        g.drawRect(odr2, odr1, delka, delka);
        for (int i = 1; i < 10; i++) {  //vykreslení mřížek
            g.drawLine(25 * i + odr1, odr1, 25 * i + odr1, odr1 + delka);   //1.pole
            g.drawLine(odr1, 25 * i + odr1, odr1 + delka, 25 * i + odr1);
            g.drawLine(25 * i + odr2, odr1, 25 * i + odr2, odr1 + delka);   //2.pole
            g.drawLine(odr2, 25 * i + odr1, odr2 + delka, 25 * i + odr1);
        }
        
        
            /*  vykreslení mých lodí a soupeřových střel  */
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(flotila[i][j] == 1){ //střela mimo
                    g.setColor(Color.BLACK);
                    g.fillOval(i*25 + odr2+9, j*25 + odr1+9, 10, 10);
                }
                else
                if(flotila[i][j] == 2){ //loď
                    g.setColor(Color.BLACK);
                    g.fillRect(i*25 + odr2, j*25 + odr1, 25, 25);
                }
                else
                if(flotila[i][j] == 3){ //zasažená loď
                    g.setColor(Color.BLACK);
                    g.fillRect(i*25 + odr2, j*25 + odr1, 25, 25);
                    g.setColor(Color.RED);
                    g.fillRect(i*25 + odr2+9, j*25 + odr1+9, 10, 10);
                }
            }
        }
        
            /*  vykreslení mých střel   */
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(radar[i][j] == 1){   //střela mimo
                    g.setColor(Color.BLACK);
                    g.fillOval(i*25 + odr1+9, j*25 + odr1+9, 10, 10);
                }
                else
                if(radar[i][j] == 2){   //zásah
                    g.setColor(Color.RED);
                    g.fillRect(i*25 + odr1+9, j*25 + odr1+9, 10, 10);
                }
            }
        }
        /*
        for (int i = 0; i < 10; i++) {  //souřadnice kolem pole
            g.drawString("" + (i+1), 25 * i + odr1 + 7, odr1 - 5);
            g.drawString(Character.toString((char)(65+i)), odr1 - 12, 25 * i + odr1 + 18);
        }*/
        
        if(!rozmisteno)
            g.fillRect(rozmisteni.mX - 10, rozmisteni.mY - 10, rozmisteni.delkaLodi * 25, rozmisteni.sirkaLodi * 25);
        else{
            hra.help.setText("");
            if(!listeneryVymeneny){
                hra.help.setText("Čekání na soupeře...");
                removeMouseMotionListener(rozmisteni);
                removeMouseWheelListener(rozmisteni);
                removeMouseListener(rozmisteni);
                while(!hraciReady){    //čekání na soupeře
                    try {
                        hra.vlaknoHry.sleep(300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Plocha.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                this.addMouseListener(strelba);
                listeneryVymeneny = true;
                hra.help.setText("Lodě rozmistěny. Klikni do pole \"RADAR\" kam chceš vystřelit.");
            }
        }
        if(rozmisteno && strelba.hraju)
            hra.help.setText("Jsi na řadě...");
        else
        if(rozmisteno && !strelba.hraju)
            hra.help.setText("Hraje soupeř...");
    }
    
}
