/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lode.semestralka;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 *
 * @author Petr Bartoš
 */
public class Lode extends JFrame {
    public JLabel help;
    /**
     * @param args the command line arguments
     */
    public Lode() {
        this.setTitle(" Lodě (hra po síti) - autor: Petr Bartoš");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(new Plocha(this));
        //this.pack();
        this.setSize(590, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        help = new JLabel("Rozmístěte lodě do pole \"VAŠE FLOTILA\".");
        help.setLocation(250, 350);
        Container kont = getContentPane();
        kont.setLayout(new FlowLayout());
        kont.add(help);
        
    }
    public static void main(String[] args) {
        
        vlaknoHry.start();
        
    }
    public static Thread vlaknoHry = new Thread(){
        @Override
        public void run(){
            new Lode().setVisible(true);
        }
    };
}
