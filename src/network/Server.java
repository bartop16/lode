/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import lode.semestralka.*;
import posluchace.*;

/**
 *
 * @author Petr Bartoš
 */
public class Server {
    protected Plocha plocha;
    protected Vystrel fire;
    
    public static final int PORT = 12345;
    ServerSocket servr;
    Socket klient;
    BufferedReader cteni;
    PrintWriter zapis;
    int xFlotily, yFlotily, xRadaru, yRadaru;
    
    public Server(Plocha plocha, Vystrel strela){
        this.plocha = plocha;
        this.fire = strela;
        try{
            this.servr = new ServerSocket(PORT);
            vlaknoServeru.start();
        }
        catch(IOException e){
            JOptionPane.showMessageDialog(null, "Chyba při vytváření socketu!\n\nProgram bude ukončen.", "Server", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    Thread vlaknoServeru = new Thread(){
        @Override
        public void run(){
            try{
                klient = servr.accept();
                cteni = new BufferedReader(new InputStreamReader(klient.getInputStream()));
                zapis = new PrintWriter(klient.getOutputStream(), true);
            }
            catch(IOException e){
                JOptionPane.showMessageDialog(null, "Chyba připojení!\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
            komunikace();
        }
    };
    
    public void komunikace(){
        String zprava = "";
        try{
            while(!plocha.rozmisteno){   //čekání na rozestavění lodí
                try {
                    vlaknoServeru.sleep(300);
                }
                catch (InterruptedException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if((zprava = cteni.readLine()).equals("READY")){ //zpráva od klienta, že je připraven
                zapis.println("READY");
                zapis.flush();
                plocha.hraciReady = true;
            }
            else
                System.out.println("Chyba, přijátá zpráva: "+ zprava);
            
                      /*    první výstřel   */
            fire.jeServer = true;
            fire.hraju = true;
                while(fire.hraju){   //čekání na výstřel
                    try {
                        vlaknoServeru.sleep(300);
                    }
                    catch (InterruptedException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                zapis.println(xRadaru +","+ yRadaru);   //odeslání souřadnic výstřelu
                zapis.flush();
                
                zprava = cteni.readLine();  //zpracování odpovědi
                if(zprava.equals("MISS"))
                    plocha.strelaRad(xRadaru, yRadaru, 0);
                else
                if(zprava.equals("HIT"))
                    plocha.strelaRad(xRadaru, yRadaru, 2);
                else
                    JOptionPane.showMessageDialog(null, "Chyba v komunikaci:\n"+ zprava, "Server", JOptionPane.WARNING_MESSAGE);
                
                refreshPlochy();
            
                      /*    herní smyčka    */
            while (!(zprava = cteni.readLine()).equals("END")){
                
                getSouradnice(zprava);  //získání souřadnic ze zprávy
                int s;
                if((s = plocha.stav(xFlotily, yFlotily)) == 0){ //zpracování a odpověd pro minutí
                    plocha.strelaFlot(xFlotily, yFlotily, s);
                    zapis.println("MISS");
                    zapis.flush();
                }
                else{       //zpracování a odpověd pro zásah
                    plocha.strelaFlot(xFlotily, yFlotily, s);
                    zapis.println("HIT");
                    zapis.flush();
                    if(plocha.konecHry()){
                        refreshPlochy();
                        zapis.println("END");
                        zapis.flush();
                        break;
                    }
                }
               
                refreshPlochy();
                
                fire.jeServer = true;    
                fire.hraju = true;
                while(fire.hraju){   //čekání na výstřel
                    try {
                        vlaknoServeru.sleep(300);
                    }
                    catch (InterruptedException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                zapis.println(xRadaru +","+ yRadaru);   //odeslání souřadnic výstřelu
                zapis.flush();
                
                zprava = cteni.readLine();  //zpracování odpovědi
                if(zprava.equals("MISS"))
                    plocha.strelaRad(xRadaru, yRadaru, 0);
                else
                if(zprava.equals("HIT"))
                    plocha.strelaRad(xRadaru, yRadaru, 2);
                else
                    JOptionPane.showMessageDialog(null, "Chyba v komunikaci:\n"+ zprava, "Server", JOptionPane.WARNING_MESSAGE);
                
                refreshPlochy();
            }
	}
	catch (SocketException e) {
            JOptionPane.showMessageDialog(null, "Spojení s klientem přerušeno.\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
	catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "Nenalezen hostitel!\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
	catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Chyba připojení!\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
        
        try{
            /*  ukončování spojení    */
            if(plocha.konecHry())
                JOptionPane.showMessageDialog(null, "Prohrál jsi.", "Konec hry   <S>", JOptionPane.INFORMATION_MESSAGE);
            else
            if(zprava.equals("END"))
                JOptionPane.showMessageDialog(null, "Vyhrál jsi! \n GRATULACE!", "Konec hry   <S>", JOptionPane.INFORMATION_MESSAGE);
            
            zapis.close();
            cteni.close();
            klient.close();
            
            if (!servr.isClosed())
                servr.close();
            
            klient.setSoTimeout(100);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Spojení ukončeno.\n\nProgram bude ukončen.", "Pozor!", JOptionPane.WARNING_MESSAGE);
            System.exit(0);
	}
    }
    
    public void getSouradnice(String str){
        String[] sourad = str.split(",");
        xFlotily = Integer.parseInt(sourad[0]);
        yFlotily = Integer.parseInt(sourad[1]);
    }
    public void setSouradnice(int a, int b){
        xRadaru = a;
        yRadaru = b;
    }
    private void refreshPlochy(){
        fire.zdroj.repaint();
    }
}
