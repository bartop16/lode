/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import lode.semestralka.*;
import posluchace.*;
/**
 *
 * @author Petr Bartoš
 */
public class Klient {
    protected Plocha plocha;
    protected Vystrel fire;
    Socket klient;
    BufferedReader cteni;
    PrintWriter zapis;
    int xFlotily, yFlotily, xRadaru, yRadaru;
    
    public Klient(Plocha plocha, Vystrel strela){
        this.plocha = plocha;
        this.fire = strela;
	final int port = 12345;
	try {
            this.klient = new Socket("localhost", port);
            
            this.zapis = new PrintWriter(klient.getOutputStream(), true);
            this.cteni = new BufferedReader(new InputStreamReader(klient.getInputStream()));
            
            vlaknoKlienta.start();
        }
        catch(IOException e){
            JOptionPane.showMessageDialog(null, "Chyba při vytváření socketu!\n\nProgram bude ukončen.", "Klient", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
    
    Thread vlaknoKlienta = new Thread(){
        @Override
        public void run(){
            komunikace();
        }
    };
    
        public void komunikace(){
            String zprava = "";
        try{
            while(!plocha.rozmisteno){   //čekání na rozestavění lodí
                try {
                    vlaknoKlienta.sleep(300);
                }
                catch (InterruptedException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            zapis.println("READY");
            zapis.flush();
            if(cteni.readLine().equals("READY")){
                plocha.hraciReady = true;
                fire.hraju = false;
            }
            
                /*    herní smyčka    */
            while (!(zprava = cteni.readLine()).equals("END")){
                
                getSouradnice(zprava);  //získání souřadnic ze zprávy
                int s;
                if((s = plocha.stav(xFlotily, yFlotily)) == 0){ //zpracování a odpověd pro minutí
                    plocha.strelaFlot(xFlotily, yFlotily, s);
                    zapis.println("MISS");
                    zapis.flush();
                }
                else{       //zpracování a odpověd pro zásah
                    plocha.strelaFlot(xFlotily, yFlotily, s);
                    zapis.println("HIT");
                    zapis.flush();
                    if(plocha.konecHry()){
                        refreshPlochy();
                        zapis.println("END");
                        zapis.flush();
                        break;
                    }
                }
                
                refreshPlochy();
                
                fire.jeServer = false;
                fire.hraju = true;
                while(fire.hraju){   //čekání na výstřel
                    try {
                        vlaknoKlienta.sleep(300);
                    }
                    catch (InterruptedException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                zapis.println(xRadaru +","+ yRadaru);   //odeslání souřadnic výstřelu
                zapis.flush();
                
                zprava = cteni.readLine();  //zpracování příchozí odpovědi
                if(zprava.equals("MISS"))
                    plocha.strelaRad(xRadaru, yRadaru, 0);
                else
                if(zprava.equals("HIT"))
                    plocha.strelaRad(xRadaru, yRadaru, 2);
                else
                    JOptionPane.showMessageDialog(null, "Chyba v komunikaci:\n"+ zprava, "Klient", JOptionPane.WARNING_MESSAGE);
                
                refreshPlochy();
            }
	}
	catch (SocketException e) {
            JOptionPane.showMessageDialog(null, "Spojení se serverem přerušeno.\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
	catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "Nenalezen hostitel!\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
	catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Chyba připojení!\n\nProgram bude ukončen.", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
	}
        
        try{
            /*  ukončování spojení    */
            if(plocha.konecHry())
                JOptionPane.showMessageDialog(null, "Prohrál jsi.", "Konec hry   <K>", JOptionPane.INFORMATION_MESSAGE);
            else
            if(zprava.equals("END"))
                JOptionPane.showMessageDialog(null, "Vyhrál jsi! \n GRATULACE!", "Konec hry   <K>", JOptionPane.INFORMATION_MESSAGE);
            
            zapis.close();
            cteni.close();
            klient.close();
            
            klient.setSoTimeout(100);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Spojení ukončeno.\n\nProgram bude ukončen.", "Pozor!", JOptionPane.WARNING_MESSAGE);
            System.exit(0);
	}
    }
    public void getSouradnice(String str){
        String[] sourad = str.split(",");
        xFlotily = Integer.parseInt(sourad[0]);
        yFlotily = Integer.parseInt(sourad[1]);
    }
    public void setSouradnice(int a, int b){
        xRadaru = a;
        yRadaru = b;
    }
    private void refreshPlochy(){
        plocha.zdrojovaKomponenta.repaint();
    }
}
