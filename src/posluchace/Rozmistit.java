/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package posluchace;

import java.awt.Component;
import java.awt.event.*;
import lode.semestralka.Plocha;

/**
 *
 * @author Petr Bartoš
 */
public class Rozmistit extends MouseAdapter {
    
    private Plocha plocha;
    public Component zdroj;
    public int mX = 0, mY = 0, delkaLodi = 5, sirkaLodi = 1;
    private int levaHrana, pravaHrana, horniHrana, dolniHrana, d = 1;
    private int[] doky = {5, 4, 4, 3, 3, 2, 2, 2};
    
    public Rozmistit(Plocha pl){
        this.plocha = pl;
        this.levaHrana = plocha.odr2;
        this.pravaHrana = plocha.odr2 + plocha.delka;
        this.horniHrana = plocha.odr1;
        this.dolniHrana = plocha.odr1 + plocha.delka;
    }
    @Override
    public void mouseDragged(MouseEvent me) {}

    @Override
    public void mouseMoved(MouseEvent me) { //lod "drží" na myši
        mX = me.getX();
        mY = me.getY();
        if((mX > levaHrana) && (mX < pravaHrana) && (mY > horniHrana) && (mY < dolniHrana)){
        zdroj = (Component)me.getSource();
        zdroj.repaint();
        }
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {  //otáčení lodí (svisle/vodorovně)
        int pom = delkaLodi;
        delkaLodi = sirkaLodi;
        sirkaLodi = pom;
        //zdroj = (Component)mwe.getSource();
        zdroj.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent me) {   //umístění lodi a zápis souřadnic do pole
        if((mX > levaHrana)&&(mX < pravaHrana)&&(mY > horniHrana)&&(mY < dolniHrana)&&(mX - 20 + 25*delkaLodi < pravaHrana)&&(mY - 20 + 25*sirkaLodi < dolniHrana)){
            //zdroj = (Component)me.getSource();
            plocha.zdrojovaKomponenta = zdroj;
            boolean volno = true;
            int i = (mX - levaHrana)/25;
            int j = (mY - horniHrana)/25;
            for (int k = 0; k < delkaLodi; k++)
                for (int l = 0; l < sirkaLodi; l++) {
                    if(plocha.flotila[i+k][j+l] != 0)
                        volno = false;
                }
            if(volno){
                for (int k = 0; k < delkaLodi; k++) {
                    for (int l = 0; l < sirkaLodi; l++) {
                        plocha.umistitLod(i+k, j+l);
                    }
                }
                if(d == 8){
                    plocha.rozmisteno = true;
                }
                else{
                delkaLodi = doky[d++];
                sirkaLodi = 1;
                }
                plocha.hra.help.setText("Umísti další loď.");
                zdroj.repaint();
            }
            else
                plocha.hra.help.setText("Lodě nesmějí jít přes sebe, zkus to znovu.");
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {}

    @Override
    public void mouseReleased(MouseEvent me) {}

    @Override
    public void mouseEntered(MouseEvent me) {}

    @Override
    public void mouseExited(MouseEvent me) {}
    
}
