/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package posluchace;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import lode.semestralka.Plocha;

/**
 *
 * @author Petr Bartoš
 */
public class Vystrel implements MouseListener {

    public Plocha plocha;
    public Component zdroj;
    public boolean hraju = false, jeServer;
    int pocHr, konHr;
    
    public Vystrel(Plocha pl){
        this.plocha = pl;
        this.pocHr = plocha.odr1;   //30
        this.konHr = plocha.odr1 + plocha.delka; //30 + 250
    }
    @Override
    public void mouseClicked(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        zdroj = (Component)me.getSource();
        if((x > pocHr) && (x < konHr) && (y > pocHr) && (y < konHr) && hraju){
            int i = (x - pocHr)/25;
            int j = (y - pocHr)/25;
            if(plocha.radar[i][j] == 0){ 
                if(jeServer){
                    plocha.soketS.setSouradnice(i, j);
                    hraju = false;
                }               
                if(!jeServer){
                    plocha.soketK.setSouradnice(i, j);
                    hraju = false;
                }
                plocha.hra.help.setText("Hraje soupeř...");
            }
            else
                plocha.hra.help.setText("Sem už jsi střílel.");
            }
    }

    @Override
    public void mousePressed(MouseEvent me) {}

    @Override
    public void mouseReleased(MouseEvent me) {}

    @Override
    public void mouseEntered(MouseEvent me) {}

    @Override
    public void mouseExited(MouseEvent me) {}

}
